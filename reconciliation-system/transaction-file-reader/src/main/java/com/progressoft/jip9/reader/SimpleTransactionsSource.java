package com.progressoft.jip9.reader;

import com.progressoft.jip9.models.TransactionDataModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SimpleTransactionsSource implements TransactionsSource{

    private final List<TransactionDataModel> dataModels;

    public SimpleTransactionsSource(List<TransactionDataModel> transactionDataModels){
        dataModels = transactionDataModels;
    }

    @Override
    public ArrayList<TransactionDataModel> getData() {

        return (ArrayList<TransactionDataModel>) dataModels;
    }

    @Override
    public void addModel(TransactionDataModel dataModel) {
        dataModels.add(dataModel);

    }
    @Override
    public void sort() {
        dataModels.sort(Comparator.comparing(TransactionDataModel::getId));
    }


}

