package com.progressoft.jip9.models;

import java.math.BigDecimal;
import java.util.Currency;

public class TransactionDataModel {
    private final String id;
    private final String date;
    private final String currencyCode;
    private BigDecimal amount;

    public TransactionDataModel(String id, String date, String currencyCode, BigDecimal amount) {
        validateArgs(id, date, currencyCode, amount);
        this.id = id;
        this.date = date;
        this.currencyCode = currencyCode;
        this.amount = amount;
        int defaultFractionDigits = Currency.getInstance(currencyCode).getDefaultFractionDigits();
        this.amount = amount.setScale(defaultFractionDigits, BigDecimal.ROUND_HALF_DOWN);


    }

    private void validateArgs(String id, String date, String currencyCode, BigDecimal amount) {
        if (id == null || id.isEmpty())
            throw new NullPointerException("Null ID");
        if (date == null || date.isEmpty())
            throw new NullPointerException("Null Date");
        if (currencyCode == null || currencyCode.isEmpty())
            throw new NullPointerException("Null Currency Code");
        if (amount == null || amount.compareTo(BigDecimal.ZERO) < 1)
            throw new IllegalArgumentException("Non Positive Amount");


    }

    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionDataModel)) return false;
        TransactionDataModel that = (TransactionDataModel) o;
        return getAmount().compareTo(that.getAmount()) == 0 &&
                getDate().equals(that.getDate()) &&
                getCurrencyCode().equals(that.getCurrencyCode());
    }

}
