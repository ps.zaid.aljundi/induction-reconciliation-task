package com.progressoft.jip9;

public interface UserDetails {
    String getUserName();
    String getUserId();
}
