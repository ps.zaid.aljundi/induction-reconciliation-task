package com.progressoft.jip9;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class AuthenticateFilter implements Filter {

    public AuthenticateFilter() {
        Logger logger = Logger.getLogger(AuthenticateFilter.class.getName());
        logger.warning("filter entered");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        String username = (String) req.getSession().getAttribute("username");
        if (username == null || !username.equalsIgnoreCase("admin")) {
            res.sendRedirect(req.getContextPath() + "/");
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {

    }
}
