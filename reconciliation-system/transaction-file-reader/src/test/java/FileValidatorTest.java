import com.progressoft.jip9.models.TransactionDataModel;
import com.progressoft.jip9.reader.CorruptedTransactionFileException;
import com.progressoft.jip9.reader.FileValidator;
import com.progressoft.jip9.reader.ReaderException;
import com.progressoft.jip9.reader.SimpleTransactionsSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileValidatorTest {
    @Test
    public void givenNullPath_whenValidate_thenFail() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> FileValidator.validatePath(null));
        Assertions.assertEquals("Null Path", nullPointerException.getMessage());
    }

    @Test
    public void givenNonExistFilePath_whenValidate_thenFail() {
        Path path = Paths.get("/home/ser/git/induction-reconciliation-task/sample-files/input-files/bank-transactions.csv");
        ReaderException readerException = Assertions.assertThrows(ReaderException.class, () -> FileValidator.validatePath(path));
        Assertions.assertEquals("Non Exist File", readerException.getMessage());
    }

    @Test
    public void givenDirectoryPath_whenValidate_thenFail() {
        Path path = Paths.get("../sample-files/input-files");
        ReaderException readerException = Assertions.assertThrows(ReaderException.class, () -> FileValidator.validatePath(path));
        Assertions.assertEquals("Not A Regular File", readerException.getMessage());
    }

    @Test
    public void givenFile_whenValidate_thenPass() throws ReaderException {
        Path path = Paths.get("../sample-files/input-files/bank-transactions.csv");
        FileValidator.validatePath(path);

    }

    @Test
    public void givenFileWithDuplicates_whenCheckDuplicates_thenFail()  {

        SimpleTransactionsSource simpleFile = new SimpleTransactionsSource(new ArrayList<>());
        simpleFile.addModel(new TransactionDataModel("tr-8787", "02/02/2020", "JOD", BigDecimal.ONE));
        simpleFile.addModel(new TransactionDataModel("tr-8787", "02/02/2020", "JOD", BigDecimal.ONE));
        CorruptedTransactionFileException corruptedTransactionFileException = Assertions.assertThrows(CorruptedTransactionFileException.class, () -> FileValidator.validateDuplicates(simpleFile));
        Assertions.assertEquals("Duplicated ID Value :tr-8787", corruptedTransactionFileException.getMessage());

    }

    @Test
    public void givenFileWithoutDuplicates_whenCheckDuplicates_thenFail()  {
        SimpleTransactionsSource simpleFile = new SimpleTransactionsSource(new ArrayList<>());
        simpleFile.addModel(new TransactionDataModel("tr-8787", "02/02/2020", "JOD", BigDecimal.ONE));
        simpleFile.addModel(new TransactionDataModel("tr-8887", "02/02/2020", "JOD", BigDecimal.ONE));
        Assertions.assertDoesNotThrow(() -> FileValidator.validateDuplicates(simpleFile));
    }
}

