package com.progressoft.jip9.compares;

import com.progressoft.jip9.models.TransactionDataModel;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CsvFilesReportGenerator implements ReportGenerator {
    private PrintWriter matchingTransactionFile;
    private PrintWriter mismatchingTransactionFile;
    private PrintWriter missingTransactionFile;
    private final Path resultPath;

    public CsvFilesReportGenerator(Path resultPath) throws WriterException {
        try {
            if (!Files.exists(resultPath))
                Files.createDirectories(resultPath);
            this.resultPath = resultPath;
            initializeFilesWriters();
            addFilesHeaders();
        } catch (IOException e) {
            throw new WriterException(e.getMessage(), e);
        }
    }

    private void addFilesHeaders() {
        matchingTransactionFile.print("transaction id,amount,currecny code,value date");
        mismatchingTransactionFile.print("found in file,transaction id,amount,currecny code,value date");
        missingTransactionFile.print("found in file,transaction id,amount,currency code,value date");
    }

    private void initializeFilesWriters() throws IOException {
        Path matchingFilePath = Paths.get(resultPath + File.separator + "Matching-Transactions-file.csv");
        matchingTransactionFile = new PrintWriter(Files.newOutputStream(matchingFilePath));
        Path mismatchingFilePath = Paths.get(resultPath + File.separator + "Mismatching-Transactions-file.csv");
        mismatchingTransactionFile = new PrintWriter(Files.newOutputStream(mismatchingFilePath));
        Path missingFilePath = Paths.get(resultPath + File.separator + "Missing-Transactions-file.csv");
        missingTransactionFile = new PrintWriter(Files.newOutputStream(missingFilePath));
    }

    @Override
    public void close() {
        flush();
        closeWriters();
    }

    private void closeWriters() {
        matchingTransactionFile.close();
        mismatchingTransactionFile.close();
        missingTransactionFile.close();
    }

    public void writeToMatchingFile(TransactionDataModel model) {
        String formattedLine = format(model);
        matchingTransactionFile.println();
        matchingTransactionFile.print(formattedLine);
        matchingTransactionFile.flush();

    }

    public void writeToMismatchingFile(TransactionDataModel model, String type) {
        String formattedLine = format(model);
        mismatchingTransactionFile.println();
        String line = type + formattedLine;
        mismatchingTransactionFile.print(line);
        mismatchingTransactionFile.flush();
    }

    public void writeToMissingFile(TransactionDataModel model, String type) {
        String formattedLine = format(model);
        missingTransactionFile.println();
        String line = type + formattedLine;
        missingTransactionFile.print(line);
        missingTransactionFile.flush();
    }

    public void flush() {
        matchingTransactionFile.flush();
        mismatchingTransactionFile.flush();
        missingTransactionFile.flush();


    }

    public String format(TransactionDataModel transactionDataModel) {
        StringBuilder extracted = getCsvLine(transactionDataModel);
        String result = extracted.toString();
        return result;
    }

    private StringBuilder getCsvLine(TransactionDataModel transactionDataModel) {
        StringBuilder extracted = new StringBuilder(transactionDataModel.getId());
        extracted.append(",");
        extracted.append(transactionDataModel.getAmount());
        extracted.append(",");
        extracted.append(transactionDataModel.getCurrencyCode());
        extracted.append(",");
        extracted.append(transactionDataModel.getDate());
        return extracted;
    }

}
