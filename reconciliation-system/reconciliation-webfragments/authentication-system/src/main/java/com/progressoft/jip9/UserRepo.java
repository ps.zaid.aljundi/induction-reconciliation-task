package com.progressoft.jip9;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.sql.*;

public class UserRepo {


    public User checkLogin(String username, String password) throws SQLException{
        password = Hashing.sha512()
                .hashString(password, StandardCharsets.UTF_8)
                .toString();
        String sql = "SELECT * FROM users WHERE user_name = ? and password = ?";
        try (Connection connection = DatabaseConnection.getConnection(); PreparedStatement statement = connection.prepareStatement(sql);) {
            statement.setString(1, username);
            statement.setString(2, password);
            // TODO result set is still open
            ResultSet result = statement.executeQuery();
            User user = null;

            if (result.next()) {
                user = new User(username, password);
            }
            result.close();
            return  user;
        }
    }
}

