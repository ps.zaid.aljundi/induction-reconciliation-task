<%@attribute name="type" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="previousShown" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="tagId" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="uploadOnclick" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="previousOnclick" rtexprvalue="true" required="true" type="java.lang.String" %>

     <div id="${tagId}"action="${pageContext.request.contextPath}/upload" method="post" enctype="multipart/form-data">
                <div class="wrap-input100 validate-input m-b-20" data-validate="Enter File Name">
                    <input class="input100" required type="text" id="${type}FileName" name="${type}FileName" placeholder="${type} name">
                    <span class="focus-input100"></span>
                </div>
                <div >
                    <label class="file">
                        <input id="${type}File" name="${type}File" class="inputfile" required type="file" value="Browse" accept=".csv,.json" aria-label="File browser example">
                        <span class="file-custom"></span>
                    </label>
                </div>
         <div >
             <label>
                 File Type :
             </label>
             <select  id ="${type}FileType" name ="${type}FileType"required type="file"  >
                 <option value="csv">CSV</option>
                 <option value="json">JSON</option>
             </select>
             <span class="focus-input100"></span>
         </div>
                <div style="display: -webkit-inline-flex" class="container-login100-form-btn">
                    <button type="button" onclick="${previousOnclick}" class="login99-form-btn" ${previousShown}>
                        Previous
                    </button>
                    <button type="button" onclick="${uploadOnclick}" class="login100-form-btn">
                      Upload
                    </button>


                </div>



            </div>

