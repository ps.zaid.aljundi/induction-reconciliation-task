package com.progressoft.jip9.compares;

import com.progressoft.jip9.reader.FileValidator;
import com.progressoft.jip9.reader.ReaderException;

import java.nio.file.Path;

public class FileDetails {
    private final Path path;
    private final String extension;

    public FileDetails(Path path, String extension) throws ReaderException {
        FileValidator.validatePath(path);
        this.path = path;
        this.extension = extension;
    }

    public Path getPath() {
        return path;
    }

    public String getExtension() {
        return extension;
    }


}
