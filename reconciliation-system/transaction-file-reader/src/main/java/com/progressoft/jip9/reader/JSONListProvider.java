package com.progressoft.jip9.reader;

import com.progressoft.jip9.models.TransactionDataModel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.List;

public class JSONListProvider implements TransactionsListProvider{
    private Path filePath;
    private SimpleTransactionsSource dataFile;


    public JSONListProvider(Path filePath, List<TransactionDataModel> transactionDataModelList) throws ReaderException {
        dataFile = new SimpleTransactionsSource(transactionDataModelList);
        FileValidator.validatePath(filePath);
        this.filePath = filePath;
    }

    private void read() throws ReaderException {
        try {
            JSONParser jsonParser = new JSONParser();
            JSONArray a = (JSONArray) jsonParser.parse(new FileReader(String.valueOf(filePath)));
            for (Object o : a) {
                TransactionDataModel dataModel = JSONobjectToModel((JSONObject) o);
                dataFile.addModel(dataModel);
            }
            dataFile.sort();
            FileValidator.validateDuplicates(dataFile);
        } catch (ParseException | IOException | CorruptedTransactionFileException | DateFormatException e) {
            throw new ReaderException(e.getMessage());
        }
    }

    @Override
    public SimpleTransactionsSource getSortedTransactionsList() throws ReaderException {
        if(isEmpty())
            read();
        return dataFile;
    }

    private boolean isEmpty() {
        return dataFile.getData().isEmpty();
    }

    private TransactionDataModel JSONobjectToModel(JSONObject jsonObject) throws DateFormatException {
        String ID = jsonObject.get("reference").toString();
        String date = jsonObject.get("date").toString();
        String currency = jsonObject.get("currencyCode").toString();
        String format = "dd/MM/yyyy";
        date = DateValidator.validate(date, format);
        BigDecimal amount = new BigDecimal(jsonObject.get("amount").toString());

        return new TransactionDataModel(ID, date, currency, amount);
    }

}
