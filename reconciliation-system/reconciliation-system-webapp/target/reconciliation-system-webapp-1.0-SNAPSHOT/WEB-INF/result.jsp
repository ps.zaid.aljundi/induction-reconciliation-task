<!DOCTYPE html>
<html lang="en">
<head>
    <title>Result</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/PapaParse/4.1.2/papaparse.js"></script>
    <script src="${pageContext.request.contextPath}/jquery/jquery-3.4.1.js"></script>
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
    <style>

        table {
            width: 100%;
            background-color: white;
            opacity: 95%;

        }

        tr {
            width: 100%
        }

        table, th {
            border: 1px solid black;
        }

        td {
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            padding: 5px;
        }

        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons that are used to open the tab content */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            width: 30%;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            background-color: white;

        }

    </style>
    <script>
        function openTab(evt, tabName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the button that opened the tab
            document.getElementById(tabName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        function arrayToTable(tableData) {
            var table = $('<table></table>');
            $(tableData).each(function (i, rowData) {
                if (rowData != "") {
                    var row = $('<tr></tr>');
                    if (i == 0)
                        row.append($('<td><h3>#</h3></td>'));
                    else
                        row.append($('<td>' + i + '</td>'));
                    $(rowData).each(function (j, cellData) {
                        console.log(cellData)
                        row.append($('<td>' + cellData + '</td>'));
                    });
                    table.append(row);
                }
            });
            return table;
        }


        $(document).ready(function () {
            $.ajax({
                type: "GET",
                url: "${pageContext.request.contextPath}/result/Match",
                success: function (data) {
                    var parsedData=null
                    if("${sessionScope["resultType"]}"=="csv"){
                      parsedData=  Papa.parse(data).data;
                    }else
                    {
                        parsedData=Papa.parse(Papa.unparse(data)).data
                    }

                    $('#Match').append(arrayToTable(parsedData));
                }
            });
            $.ajax({
                type: "GET",
                url: "${pageContext.request.contextPath}/result/Mismatch",
                success: function (data) {
                    var parsedData=null
                    if("${sessionScope["resultType"]}"=="csv"){
                        parsedData=  Papa.parse(data).data;
                    }else
                    {
                        parsedData=Papa.parse(Papa.unparse(data)).data
                    }
                    $('#Mismatch').append(arrayToTable(parsedData));
                }
            });
            $.ajax({
                type: "GET",
                url: "${pageContext.request.contextPath}/result/Missing",
                success: function (data) {
                    var parsedData=null
                    if("${sessionScope["resultType"]}"=="csv"){
                        parsedData=  Papa.parse(data).data;
                    }else
                    {
                        parsedData=Papa.parse(Papa.unparse(data)).data
                    }
                    $('#Missing').append(arrayToTable(parsedData));
                }
            });
            $('#Match').show()
        });

        function downloadFile() {
            let fileName = document.getElementsByClassName("active").item(0).innerHTML;
            let fileType = "${sessionScope['resultType']}";
            var a = document.createElement('a');
            a.download = fileName + "." + fileType;
            a.href = "${pageContext.request.contextPath}/result/" + fileName;
            a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
            a.click();
            console.log(a);
        }



        function reCompare() {
            location.replace("/upload")
        }
    </script>
</head>
<body>
<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
    <div style="display: block;width: 100%;">
        <div class="tab">
            <button class="tablinks active" onclick="openTab(event, 'Match')">Match</button>
            <button class="tablinks" onclick="openTab(event, 'Mismatch')">Mismatch</button>
            <button class="tablinks" onclick="openTab(event, 'Missing')">Missing</button>
        </div>

        <!-- Tab content -->

        <div id="Match" class="active tabcontent">

        </div>

        <div id="Mismatch" class="tabcontent">

        </div>

        <div id="Missing" class="tabcontent">

        </div>
        <div style="margin-right:50px;margin-top: 10px;" class="container-login100-form-btn">
            <button class="login99-form-btn" onclick="reCompare()" style="color:#1d1d1d;background-color: #ffdcd0"
                    type="button">
                Compare new files
            </button>
            <button class="login100-form-btn" type="button" onclick="downloadFile()">
                Download
            </button>
        </div>
    </div>
</div>
</body>
</html>