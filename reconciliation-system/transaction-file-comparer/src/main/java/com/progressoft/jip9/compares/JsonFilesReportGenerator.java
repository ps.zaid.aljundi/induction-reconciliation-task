package com.progressoft.jip9.compares;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.progressoft.jip9.models.TransactionDataModel;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JsonFilesReportGenerator implements ReportGenerator<JsonObject> {
    private final Gson gson;

    private PrintWriter matchingTransactionFile;
    private PrintWriter mismatchingTransactionFile;
    private PrintWriter missingTransactionFile;

    private JsonArray matchingTransactionsArray;
    private JsonArray misMatchingTransactionsArray;
    private JsonArray missingTransactionsArray;
    private final Path resultPath;

    public JsonFilesReportGenerator(Path resultPath) throws WriterException {
        try {
            if (!Files.exists(resultPath))
                Files.createDirectories(resultPath);
            this.resultPath = resultPath;
            gson = new GsonBuilder().setPrettyPrinting().create();
            initializeArrays();

        } catch (IOException e) {
            throw new WriterException(e.getMessage(), e);
        }
    }

    private void initializeArrays() {
        matchingTransactionsArray = new JsonArray();
        misMatchingTransactionsArray = new JsonArray();
        missingTransactionsArray = new JsonArray();
    }

    private void initializeFileWriters(Path resultPath) throws IOException {
        Path matchingFilePath = Paths.get(resultPath + File.separator + "Matching-Transactions-file.json");
        matchingTransactionFile = new PrintWriter(Files.newOutputStream(matchingFilePath));
        Path mismatchingFilePath = Paths.get(resultPath + File.separator + "Mismatching-Transactions-file.json");
        mismatchingTransactionFile = new PrintWriter(Files.newOutputStream(mismatchingFilePath));
        Path missingFilePath = Paths.get(resultPath + File.separator + "Missing-Transactions-file.json");
        missingTransactionFile = new PrintWriter(Files.newOutputStream(missingFilePath));
    }

    @Override
    public void writeToMatchingFile(TransactionDataModel model) {
        addModel(model, matchingTransactionsArray);
    }


    @Override
    public void writeToMismatchingFile(TransactionDataModel model, String type) {
        addModelWithType(model, type, misMatchingTransactionsArray);

    }

    @Override
    public void writeToMissingFile(TransactionDataModel model, String type) {
        addModelWithType(model, type, missingTransactionsArray);

    }

    @Override
    public JsonObject format(TransactionDataModel model) {
        String s = gson.toJson(model, TransactionDataModel.class);
        JsonObject jsonObject = gson.fromJson(s, JsonObject.class);
        return jsonObject;

    }

    @Override
    public void close() {

        try {
            initializeFileWriters(resultPath);
            printArraysAndFlush();
            closeWriters();
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private void closeWriters() {
        matchingTransactionFile.close();
        mismatchingTransactionFile.close();
        missingTransactionFile.close();
    }

    private void printArraysAndFlush() {
        matchingTransactionFile.print(matchingTransactionsArray.getAsJsonArray());
        mismatchingTransactionFile.print(misMatchingTransactionsArray.getAsJsonArray());
        missingTransactionFile.print(missingTransactionsArray.getAsJsonArray());
        matchingTransactionFile.flush();
        mismatchingTransactionFile.flush();
        missingTransactionFile.flush();
    }

    private void addModel(TransactionDataModel model, JsonArray transactionsArray) {

        transactionsArray.add(format(model));
    }

    private void addModelWithType(TransactionDataModel model, String type, JsonArray transactionsArray) {
        JsonObject format = format(model);
        StringBuilder stringBuilder = new StringBuilder(format.toString());
        stringBuilder.insert(1, "\"Found in file\":\"" + type.substring(0, type.length() - 1) + "\",");
        JsonObject jsonObject = gson.fromJson(stringBuilder.toString(), JsonObject.class);
        transactionsArray.add(jsonObject);
    }
}
