
import com.progressoft.jip9.reader.DateFormatException;
import com.progressoft.jip9.reader.DateValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateValidatorTest {
    @Test
    public void givenWrongDateFormat_WhenValidate_ThenFail(){
        DateFormatException dateFormatException = Assertions.assertThrows(DateFormatException.class, () -> DateValidator.validate("02/10/2020", "dd-MM-yyyy"));
        Assertions.assertEquals("Unparseable date: \"02/10/2020\" With Format dd-MM-yyyy",dateFormatException.getMessage());
    }
}
