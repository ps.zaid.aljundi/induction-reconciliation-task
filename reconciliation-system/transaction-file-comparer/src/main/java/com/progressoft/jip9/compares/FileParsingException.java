package com.progressoft.jip9.compares;

public class FileParsingException extends RuntimeException {
    public FileParsingException(String s) {
        super(s);
    }
}

