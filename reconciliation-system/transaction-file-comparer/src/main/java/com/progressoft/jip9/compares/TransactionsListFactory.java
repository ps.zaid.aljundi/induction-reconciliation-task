package com.progressoft.jip9.compares;

import com.progressoft.jip9.reader.*;

import java.nio.file.Path;
import java.util.ArrayList;

public class TransactionsListFactory {
    public static TransactionsSource newTransactionsList(FileDetails fileDetails) throws ReaderException, FileParsingException {

        TransactionsListProvider reader = getTransactionsListProvider(fileDetails);
        return reader.getSortedTransactionsList();
    }

    private static TransactionsListProvider getTransactionsListProvider(FileDetails fileDetails) throws ReaderException, FileParsingException {
        String extension = fileDetails.getExtension();
        Path path = fileDetails.getPath();
        if (extension.equalsIgnoreCase("CSV")) {
            return new CSVListProvider(path , new ArrayList<>());
        } else if (extension.equalsIgnoreCase("JSON")) {
            return new JSONListProvider(path, new ArrayList<>());
        } else throw new FileParsingException("Not Supported File Extension");
    }
}
