package com.progressoft.jip9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class MockUPAuthenticatorTest {
    static MockUPAuthenticator mockUPAuthenticator;
    @BeforeAll
    public static void initialize(){
        mockUPAuthenticator=new MockUPAuthenticator();

    }
    @Test
    public void giveNullTokensArgs_whenAuthenticate_thenFail(){
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> mockUPAuthenticator.authenticate(null));
        Assertions.assertEquals("Null Tokens Args", nullPointerException.getMessage());
    }
    @Test
    public void giveNullToken_whenAuthenticate_thenFail(){
        String []tokens=new String[]{"admin",null};
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> mockUPAuthenticator.authenticate(tokens));
        Assertions.assertEquals("Null Token Value", nullPointerException.getMessage());

    }
    @Test
    public void givenTokensArgsWithWrongLength_whenAuthenticate_thenFail(){
        String []tokens=new String[]{"admin","admin","admin"};
        Assertions.assertThrows(IllegalArgumentException.class, () -> mockUPAuthenticator.authenticate(tokens));
        String []tokens1=new String[]{"admin"};
        Assertions.assertThrows(IllegalArgumentException.class, () -> mockUPAuthenticator.authenticate(tokens1));
        String []tokens2=new String[]{};
        Assertions.assertThrows(IllegalArgumentException.class, () -> mockUPAuthenticator.authenticate(tokens1));

    }
    @Test
    public void givenWrongCredintials_whenAuthenticate_thenReturnFalse() {
        Assertions.assertNull(mockUPAuthenticator.authenticate("admin1","admin"));
    }

    @Test
    public void givenCorrenctCredintials_whenAuthenticate_thenReturnTrue() {
        UserDetails authenticate = mockUPAuthenticator.authenticate("admin", "admin");
        Assertions.assertNotNull(authenticate);
        Assertions.assertEquals("admin",authenticate.getUserName());

    }
}
