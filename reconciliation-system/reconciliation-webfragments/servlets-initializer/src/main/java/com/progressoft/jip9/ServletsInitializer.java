package com.progressoft.jip9;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class ServletsInitializer implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        registerAuthenticator(servletContext);
        registerUploader(servletContext);
        registerComparer(servletContext);
        registerRender(servletContext);
        registerDownload(servletContext);
        registerSessionExpiredServlet(servletContext);
        registerLogoutServlet(servletContext);
    }

    private void registerSessionExpiredServlet(ServletContext ctx) {
        SessionExpiredServlet sessionExpiredServlet = new SessionExpiredServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("sessionExpiredServlet", sessionExpiredServlet);
        registration.addMapping("/session");
    }
    private void registerLogoutServlet(ServletContext ctx) {
        LogoutServlet logoutServlet = new LogoutServlet();
        ServletRegistration.Dynamic registration = ctx.addServlet("logoutServlet", logoutServlet);
        registration.addMapping("/Login");
    }

    private void registerDownload(ServletContext servletContext) {
        ResultServlet renderServlet=new ResultServlet();
        ServletRegistration.Dynamic resultFiles = servletContext.addServlet("resultFiles", renderServlet);
        resultFiles.addMapping("/result/*");
    }

    private void registerRender(ServletContext servletContext) {
        RenderServlet renderServlet=new RenderServlet();
        ServletRegistration.Dynamic render = servletContext.addServlet("render", renderServlet);
        render.addMapping("/result");
    }

    private void registerComparer(ServletContext servletContext) {
        FilesItemsComparer filesItemsComparer=new FilesItemsComparer();
        CompareServlet compareServlet = new CompareServlet(filesItemsComparer);
        ServletRegistration.Dynamic comparer = servletContext.addServlet("comparer", compareServlet);
        comparer.addMapping("/compare");
    }

    private void registerUploader(ServletContext servletContext) {
        UploadServlet uploadServlet = new UploadServlet();
        ServletRegistration.Dynamic uploader = servletContext.addServlet("uploader", uploadServlet);
        uploader.addMapping("/upload");
    }

    private void registerAuthenticator(ServletContext servletContext) {
        AuthenticateServlet authenticateServlet = new AuthenticateServlet(new UserRepo());
        ServletRegistration.Dynamic authenticator = servletContext.addServlet("authenticator", authenticateServlet);
        authenticator.addMapping("/login");
    }
}
