package com.progressoft.jip9.reader;

public interface TransactionsListProvider {
    TransactionsSource getSortedTransactionsList() throws ReaderException;
}
