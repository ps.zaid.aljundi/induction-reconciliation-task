package com.progressoft.jip9.reader;

import java.text.ParseException;

public class DateFormatException extends RuntimeException {
    public DateFormatException(String message, ParseException e) {
        super(message,e);
    }
}

