package com.progressoft.jip9;

import java.nio.file.Path;
import java.util.List;

public interface FilesComparer<T> {
    void doCompare(Path path, List<T> list) throws FilesException;

}
