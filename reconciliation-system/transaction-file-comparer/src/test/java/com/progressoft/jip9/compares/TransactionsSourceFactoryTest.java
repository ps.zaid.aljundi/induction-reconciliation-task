package com.progressoft.jip9.compares;

import com.progressoft.jip9.models.TransactionDataModel;
import com.progressoft.jip9.reader.ReaderException;
import com.progressoft.jip9.reader.TransactionsSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

public class TransactionsSourceFactoryTest {
    @Test
    public void canCreate() {
        TransactionsListFactory transactionsListFactory = new TransactionsListFactory();

    }

    @Test
    public void givenCSVFileDetails_whenApplying_thenReturnContentSimpleFile() throws IOException, ReaderException, FileParsingException {
        Path temp = Files.createTempFile("F1",".csv");
        try (PrintWriter printWriter = new PrintWriter(Files.newOutputStream(temp));) {
            printWriter.write("trans unique id,trans description,amount,currecny,purpose,value date,trans type\n" +
                    "TR-47884222206,atm withdrwal,500.0,USD,,2020-02-10,D");
            printWriter.flush();
        }
        String extension = "CSV";
        FileDetails fileDetails = new FileDetails(temp, extension);
        TransactionsSource file = TransactionsListFactory.newTransactionsList(fileDetails);
        int size = file.getData().size();
        Assertions.assertEquals(1, size);
        ArrayList<TransactionDataModel> arrayList = new ArrayList<>();
        arrayList.add(new TransactionDataModel("TR-47884222206", "2020-02-10", "USD", new BigDecimal(500).setScale(4)));
        Assertions.assertEquals(arrayList, file.getData());


    }
    @Test
    public void givenNonKnownExtension_whenApplying_thenFail() throws IOException, ReaderException {
        Path temp = Files.createTempFile("F1",".csv");
        try (PrintWriter printWriter = new PrintWriter(Files.newOutputStream(temp));) {
            printWriter.write("trans unique id,trans description,amount,currecny,purpose,value date,trans type\n" +
                    "TR-47884222206,atm withdrwal,500.0,USD,,2020-02-10,D");
            printWriter.flush();
        }
        String extension = "CSS";
        FileDetails fileDetails = new FileDetails(temp, extension);
        FileParsingException fileParsingException = Assertions.assertThrows(FileParsingException.class, () -> TransactionsListFactory.newTransactionsList(fileDetails));
        Assertions.assertEquals("Not Supported File Extension",fileParsingException.getMessage());
    }
}
