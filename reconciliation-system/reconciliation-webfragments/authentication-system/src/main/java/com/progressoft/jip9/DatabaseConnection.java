package com.progressoft.jip9;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseConnection {
    private static final String url = "jdbc:mysql://localhost:3306/reconciliation?serverTimezone=UTC";
    private static final String name = "root";
    private static final String password = "root";


    public static Connection getConnection() {

        try {
            return DriverManager.getConnection(url, name, password);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("couldn't connect!");
            throw new CanNotAccessDBExeption();
        }
    }

    public static void failIfNull(String url, String name) {
        if (url == null)
            throw new NullPointerException("url can't be null");
        if (name == null)
            throw new NullPointerException("name can't be null");


    }
}
