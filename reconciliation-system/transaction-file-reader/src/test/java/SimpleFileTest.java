import com.progressoft.jip9.models.TransactionDataModel;
import com.progressoft.jip9.reader.SimpleTransactionsSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;

public class SimpleFileTest {

    @Test
    public void givenCSVFile_whenGetSize_thenReturnZero() {
        Path path = Paths.get("/home/user/git/induction-reconciliation-task/sample-files/input-files/bank-transactions.csv");
        SimpleTransactionsSource simpleFile = new SimpleTransactionsSource(new ArrayList<>());
        int size = simpleFile.getData().size();
        Assertions.assertEquals(0, size);
    }

    @Test
    public void givenFilledCSVFile_whenGetData_thenReturnCorrectData
            () throws FileNotFoundException {
        SimpleTransactionsSource simpleFile = new SimpleTransactionsSource(new ArrayList<>());
        ArrayList<TransactionDataModel> arrayList = new ArrayList<>();
        TransactionDataModel dataModel = new TransactionDataModel("1551", "12/12/2012", "JOD", new BigDecimal(45));
        simpleFile.addModel(dataModel);
        int size = simpleFile.getData().size();
        arrayList.add(dataModel);
        Assertions.assertEquals(1, size);
        Assertions.assertEquals(simpleFile.getData(), arrayList);
        TransactionDataModel dataModel1 = new TransactionDataModel("1555", "12/12/2012", "JOD", new BigDecimal(45));
        simpleFile.addModel(dataModel1);
        size = simpleFile.getData().size();
        Assertions.assertEquals(2, size);
        arrayList.add(dataModel1);
        Assertions.assertEquals(simpleFile.getData(), arrayList);
        simpleFile.sort();
        arrayList.sort(Comparator.comparing(TransactionDataModel::getId));
        Assertions.assertEquals(arrayList,simpleFile.getData());

    }


}
