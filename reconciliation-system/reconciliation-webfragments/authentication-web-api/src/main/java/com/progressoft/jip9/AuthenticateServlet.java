package com.progressoft.jip9;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class AuthenticateServlet extends HttpServlet {
    private final UserRepo userRepo;

    public AuthenticateServlet(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws  IOException  {
        try {
           User user = userRepo.checkLogin(req.getParameter("username"),req.getParameter("password"));
            if (user!=null) {
                req.getSession().setAttribute("username", user.getUsername());
                resp.getWriter().print("success");

                return;
            }
            resp.getWriter().print("failed");
        } catch (SQLException throwables) {
            // TODO what if something else
            throwables.printStackTrace();
        }

    }
}
