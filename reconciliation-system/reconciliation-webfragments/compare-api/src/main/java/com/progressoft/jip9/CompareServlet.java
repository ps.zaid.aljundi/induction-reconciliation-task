package com.progressoft.jip9;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class CompareServlet extends HttpServlet {
    private final FilesComparer filesComparer;

    public CompareServlet(FilesComparer filesComparer) {
        this.filesComparer = filesComparer;
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.changeSessionId();
        FileItemFactory itemFactory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(itemFactory);
        upload(req, resp, upload);
    }

    private void upload(HttpServletRequest req, HttpServletResponse resp, ServletFileUpload upload) throws IOException, FilesException {
        try {
            List<FileItem> items = upload.parseRequest(req);
            Path uploadDirectory = Files.createTempDirectory("tmp");
            filesComparer.doCompare(uploadDirectory, items);
            req.getSession().setAttribute("resultDirectory", uploadDirectory);
            req.getSession().setAttribute("resultType", FilesItemsComparer.getString(items, 6));
            resp.sendRedirect("result");
        } catch (FileUploadException e) {
            throw new FilesException("can't parse request ", e);
        }
    }
}
