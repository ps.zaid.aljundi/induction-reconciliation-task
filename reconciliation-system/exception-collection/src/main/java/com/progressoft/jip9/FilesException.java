package com.progressoft.jip9;

import org.apache.commons.fileupload.FileUploadException;

import java.io.IOException;

public class FilesException extends RuntimeException {
    public FilesException(String message, FileUploadException e) {
        super(message,e);
    }

    public FilesException(String message, IOException e) {
        super(message,e);
    }

    public FilesException(String message, Exception e) {
        super(message,e);
    }
}
