package com.progressoft.jip9;

import com.progressoft.jip9.compares.*;
import com.progressoft.jip9.reader.ReaderException;
import com.progressoft.jip9.reader.TransactionsSource;

import java.net.URI;
import java.nio.file.Paths;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception  {
        Scanner input = new Scanner(System.in);
        System.out.println(">> Enter source file location:");
        String sourcePath = input.nextLine();
        System.out.println(">> Enter source file format:");
        String sourceFormat = input.nextLine();
        System.out.println(">> Enter target file location:");
        String targetPath = input.nextLine();
        System.out.println(">> Enter target file format:");
        String targetFormat = input.nextLine();
        doCompare(sourcePath, sourceFormat, targetPath, targetFormat);


    }

    private static void doCompare(String sourcePath, String sourceFormat, String targetPath, String targetFormat) throws ReaderException, FileParsingException, Exception, WriterException {
        FileDetails sourceDetails = new FileDetails(Paths.get(sourcePath), sourceFormat);
        FileDetails targetDetails = new FileDetails(Paths.get(targetPath), targetFormat);

        TransactionsSource sourceFile = TransactionsListFactory.newTransactionsList(sourceDetails);
        TransactionsSource targetFile = TransactionsListFactory.newTransactionsList(targetDetails);
        TransactionsComparer transactionsComparer = new TransactionsComparer(new CsvFilesReportGenerator(Paths.get("/home/user/induction-reconciliation-task/induction-reconciliation-task/sample-files/result-files")));
        transactionsComparer.compare(sourceFile, targetFile);
        URI resultDirectoryUri = Paths.get("/home/user/induction-reconciliation-task/induction-reconciliation-task/sample-files/result-files").toUri();
        System.out.println("Reconciliation finished.\n" +
                "Result files are availble in directory " + resultDirectoryUri);
    }
}
