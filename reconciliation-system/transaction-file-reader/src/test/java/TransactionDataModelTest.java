import com.progressoft.jip9.models.TransactionDataModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class TransactionDataModelTest {
    @Test
    public void canCreate() {
        TransactionDataModel TransactionDataModel = new TransactionDataModel("747", "10/10/2012", "JOD", new BigDecimal(1.0));
    }
    @Test
    public void givenNullID_whenConstructing_thenFail() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> new TransactionDataModel(null, "5/10/1998", "", new BigDecimal("1.0")));
        Assertions.assertEquals("Null ID", nullPointerException.getMessage());

    }

    @Test
    public void givenNullDate_whenConstructing_thenFail() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> new TransactionDataModel("12215", null, "JOD", new BigDecimal("1.0")));
        Assertions.assertEquals("Null Date", nullPointerException.getMessage());

    }

    @Test
    public void givenNullCurrencyCode_whenConstructing_thenFail() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> new TransactionDataModel("12215", "25/10/2005", null, new BigDecimal("1.0")));
        Assertions.assertEquals("Null Currency Code", nullPointerException.getMessage());

    }

    @Test
    public void givenNonPositiveAmount_whenConstructing_thenFail() {
        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TransactionDataModel("12215", "25/10/2005", "JOD", new BigDecimal(0)));
        Assertions.assertEquals("Non Positive Amount", illegalArgumentException.getMessage());
        illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TransactionDataModel("12215", "25/10/2005", "JOD", new BigDecimal(-7)));
        Assertions.assertEquals("Non Positive Amount", illegalArgumentException.getMessage());
    }

    @Test
    public void givenValidValues_whenConstructing_thenGetCorrectValues() {
        TransactionDataModel TransactionDataModel = new TransactionDataModel("12215", "25/10/2005", "JOD", new BigDecimal(1.0));
        Assertions.assertEquals("12215", TransactionDataModel.getId());
        Assertions.assertEquals("25/10/2005", TransactionDataModel.getDate());
        Assertions.assertEquals("JOD", TransactionDataModel.getCurrencyCode());
        Assertions.assertEquals(new BigDecimal("1.0").setScale(3), TransactionDataModel.getAmount());

    }

    @Test
    public void givenTransactionDataModel_whenCallToString_thenReturnID() {
        TransactionDataModel TransactionDataModel = new TransactionDataModel("12215", "25/10/2005", "JOD", new BigDecimal(1.0));
        Assertions.assertEquals("12215",TransactionDataModel.toString());

    }
    @Test
    public void givenTwoTransactionDataModel_whenCallEquals_thenReturnTrue() {
        TransactionDataModel TransactionDataModel = new TransactionDataModel("12215", "25/10/2005", "JOD", new BigDecimal(1.0));
        TransactionDataModel TransactionDataModel1 = new TransactionDataModel("124785", "25/10/2005", "JOD", new BigDecimal(1.0));
        Assertions.assertEquals(TransactionDataModel,TransactionDataModel1);

    }

}
