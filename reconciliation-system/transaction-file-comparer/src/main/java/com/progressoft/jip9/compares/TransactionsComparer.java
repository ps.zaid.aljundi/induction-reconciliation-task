package com.progressoft.jip9.compares;

import com.progressoft.jip9.models.TransactionDataModel;
import com.progressoft.jip9.models.TransactionDataModelComparator;
import com.progressoft.jip9.reader.TransactionsSource;

import java.util.ArrayList;
import java.util.Collections;

public class TransactionsComparer {
    private final ReportGenerator reportGenerator;
    public TransactionsComparer(ReportGenerator reportGenerator){
        this.reportGenerator=reportGenerator;
    }

    public void compare(TransactionsSource... lists)  {
        TransactionsSource source=lists[0];
        TransactionsSource target=lists[1];
        ArrayList<TransactionDataModel> sourceData = source.getData();
        ArrayList<TransactionDataModel> targetData = target.getData();
        for (TransactionDataModel sourceModel : sourceData) {
            int matchIndex = findMatch(targetData, sourceModel);
            writeToSuitedFile(reportGenerator, targetData, sourceModel, matchIndex);
        }
        for (TransactionDataModel targetModel : targetData) {
            reportGenerator.writeToMissingFile(targetModel, "TARGET,");
        }

    }

    private static void writeToSuitedFile(ReportGenerator filesReportGenerator, ArrayList<TransactionDataModel> targetData, TransactionDataModel sourceModel, int matchIndex) {
        if (matchIndex < 0) {
            filesReportGenerator.writeToMissingFile(sourceModel,"SOURCE," );
            return;
        }
        TransactionDataModel targetModel = targetData.remove(matchIndex);

        compareThenWrite(filesReportGenerator, sourceModel, targetModel);
    }

    private static void compareThenWrite(ReportGenerator filesReportGenerator, TransactionDataModel sourceModel,TransactionDataModel targetModel) {
        if (sourceModel.equals(targetModel)) {
            filesReportGenerator.writeToMatchingFile(sourceModel);
            return;
        }
        filesReportGenerator.writeToMismatchingFile( sourceModel,"SOURCE,");
        filesReportGenerator.writeToMismatchingFile( targetModel,"TARGET,");
    }



    private static int findMatch(ArrayList<TransactionDataModel> targetData, TransactionDataModel sourceModel) {
        return Collections.binarySearch(targetData, sourceModel, new TransactionDataModelComparator());
    }


}
