package com.progressoft.jip9.reader;

public class CorruptedTransactionFileException extends RuntimeException {
    public CorruptedTransactionFileException(String s) {
        super(s);
    }
}


