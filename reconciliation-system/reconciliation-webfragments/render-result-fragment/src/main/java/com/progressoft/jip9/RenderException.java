package com.progressoft.jip9;

import javax.servlet.ServletException;

public class RenderException extends RuntimeException {
    public RenderException(String s, ServletException e) {
        super(s,e);
    }
}
