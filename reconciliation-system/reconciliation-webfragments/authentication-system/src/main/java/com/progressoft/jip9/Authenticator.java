package com.progressoft.jip9;

public interface Authenticator<T> {

    UserDetails authenticate(T[] tokens);
}
