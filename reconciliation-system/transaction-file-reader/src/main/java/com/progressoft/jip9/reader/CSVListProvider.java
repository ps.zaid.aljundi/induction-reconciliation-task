package com.progressoft.jip9.reader;

import com.progressoft.jip9.models.TransactionDataModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CSVListProvider implements TransactionsListProvider {
    private final SimpleTransactionsSource dataFile;
    private final Path filePath;

    public CSVListProvider(Path filePath, List<TransactionDataModel> list) throws ReaderException {
        FileValidator.validatePath(filePath);
        this.filePath = filePath;
        dataFile = new SimpleTransactionsSource(list);
    }


    private void read() throws ReaderException {
        try (BufferedReader bufferedReader = Files.newBufferedReader(filePath)) {
            String firstLine = bufferedReader.readLine();
            readWithHeader(bufferedReader, firstLine);
        } catch (ReaderException | IOException e) {
            throw new ReaderException(e.getMessage(),  e);
        }
    }

    @Override
    public TransactionsSource getSortedTransactionsList() throws ReaderException {
        if(isEmpty())
            read();
        return dataFile;
    }

    private boolean isEmpty() {
        return dataFile.getData().isEmpty();
    }

    private void readWithHeader(BufferedReader bufferedReader, String firstLine) throws IOException, CorruptedTransactionFileException, ReaderException, DateFormatException {
        List<String> headers =
                Arrays.stream(firstLine.split(","))
                        .collect(Collectors.toList());
        int idIndex = headers.indexOf("trans unique id");
        int amountIndex = headers.indexOf("amount");
        int currencyIndex = headers.indexOf("currecny");
        int dateIndex = headers.indexOf("value date");
        validateIndices(idIndex, amountIndex, currencyIndex, dateIndex);
        addRows(bufferedReader, idIndex, amountIndex, currencyIndex, dateIndex);
        dataFile.sort();
        FileValidator.validateDuplicates(dataFile);
    }

    private void validateIndices(int IDIndex, int amountIndex, int currencyIndex, int dateIndex) throws ReaderException {
        if (IDIndex < 0)
            throw new ReaderException("trans unique id Column Not Found");
        if (amountIndex < 0)
            throw new ReaderException("amount Column Not Found");
        if (currencyIndex < 0)
            throw new ReaderException("currecny Column Not Found");
        if (dateIndex < 0)
            throw new ReaderException("value date Column Not Found");
    }

    private void addRows(BufferedReader bufferedReader, int idIndex, int amountIndex, int currencyIndex, int dateIndex) throws IOException, DateFormatException {
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            String[] splitted = line.split(",");
            String date = splitted[dateIndex];
            String format = "yyyy-MM-dd";
            date = DateValidator.validate(date, format);
            String id = splitted[idIndex];
            BigDecimal amount = new BigDecimal(splitted[amountIndex]);
            String currencyCode = splitted[currencyIndex];
            dataFile.addModel(new TransactionDataModel(id, date, currencyCode, amount));

        }
    }


}


