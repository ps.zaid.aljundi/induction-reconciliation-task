package com.progressoft.jip9.compares;

public class WriterException extends RuntimeException {
    public WriterException(String s, Exception e) {
        super(s,e);
    }
}
