package com.progressoft.jip9.reader;

import com.progressoft.jip9.models.TransactionDataModel;

import java.util.ArrayList;

public interface TransactionsSource {
    ArrayList<TransactionDataModel> getData();
    void addModel(TransactionDataModel dataModel);
    void sort();


}
