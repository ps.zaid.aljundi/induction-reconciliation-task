package com.progressoft.jip9.reader;

public class ReaderException extends RuntimeException {
    public ReaderException(String e) {
        super(e);
    }

    public ReaderException(String message, Exception e) {
        super(message, e);
    }
}

