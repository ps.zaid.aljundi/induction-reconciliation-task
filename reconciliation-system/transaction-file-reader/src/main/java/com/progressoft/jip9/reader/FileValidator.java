package com.progressoft.jip9.reader;

import com.progressoft.jip9.models.TransactionDataModel;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

public class FileValidator {
    public static void validatePath(Path filePath) throws ReaderException {
        if (isNull(filePath))
            throw new NullPointerException("Null Path");
        if (!isExists(filePath))
            throw new ReaderException("Non Exist File");
        if (!Files.isRegularFile(filePath))
            throw new ReaderException("Not A Regular File");

    }

    public static void validateDuplicates(TransactionsSource file) throws CorruptedTransactionFileException {
        ArrayList<TransactionDataModel> sourceData=file.getData();
        int sourceSize = sourceData.size();
        for (int index = 0; index < sourceSize - 1; index++) {
            String currentID = sourceData.get(index).getId();
            String nextID = sourceData.get(index + 1).getId();
            if (currentID.equals(nextID)) {
                throw new CorruptedTransactionFileException("Duplicated ID Value :" + currentID);
            }
        }
    }


    private static boolean isNull(Path filePath) {
        return filePath == null;
    }

    private static boolean isExists(Path filePath) {
        return Files.exists(filePath);
    }
}

