package com.progressoft.jip9.compares;

import com.progressoft.jip9.models.TransactionDataModel;

public interface ReportGenerator<T>  extends AutoCloseable{
    void writeToMatchingFile(TransactionDataModel model);

    void writeToMismatchingFile(TransactionDataModel model, String Type);

    void writeToMissingFile(TransactionDataModel model, String type);

    T format(TransactionDataModel transactionDataModel);

}
