package com.progressoft.jip9;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ResultServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        Path parentDirectory = Paths.get(session.getAttribute("resultDirectory").toString());
        String fileType = req.getPathInfo();
        String fileExtension = (String) session.getAttribute("resultType");
        String fileName = getFileName(fileType, fileExtension);
        String trimmedFileType = fileType.substring(1);

        writeResponse(resp, parentDirectory, fileName, trimmedFileType, fileExtension);

    }

    private String getFileName(String fileType, String fileExtension) {
        String fileName;
        switch (fileType) {
            case "/Match": {
                fileName = "/Matching-Transactions-file." + fileExtension;
                break;
            }
            case "/Mismatch": {
                fileName = "/Mismatching-Transactions-file." + fileExtension;
                break;
            }
            case "/Missing": {
                fileName = "/Missing-Transactions-file." + fileExtension;
                break;
            }
            default:
                throw new UnknownFileTypeException("Unknown Type :" + fileType);
        }
        return fileName;
    }


    private void writeResponse(HttpServletResponse resp, Path parentDirectory, String fileName, String fileType, String fileExtension) throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(parentDirectory + File.separator + fileName)))) {
            ServletOutputStream printWriter = resp.getOutputStream();
            resp.setContentType("application/" + fileType);
            resp.setHeader("Content-Disposition", "attachment; filename=" + fileType + "." + fileExtension);
            String s;
            while ((s = bufferedReader.readLine()) != null) {
                printWriter.println(s);
            }
            printWriter.flush();
        }
    }
}
