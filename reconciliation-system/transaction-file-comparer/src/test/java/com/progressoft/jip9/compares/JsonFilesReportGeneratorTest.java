package com.progressoft.jip9.compares;

import com.google.gson.JsonObject;
import com.progressoft.jip9.models.TransactionDataModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.Paths;

public class JsonFilesReportGeneratorTest {
    @Test
    public void givenTransactionModel_whenFormat_thenReturnJsonObject() throws WriterException {
        TransactionDataModel TransactionDataModel = new TransactionDataModel("Tr-12454", "2020-01-31", "JOD", BigDecimal.ONE);
        JsonFilesReportGenerator jsonFilesReportGenerator = new JsonFilesReportGenerator(Paths.get("./target"));
        String expectedResult = "{" +
                "\"id\":\"Tr-12454\"," +
                "\"date\":\"2020-01-31\"," +
                "\"currencyCode\":\"JOD\"," +
                "\"amount\":1.000" +
                "}";
        JsonObject format = jsonFilesReportGenerator.format(TransactionDataModel);
        Assertions.assertEquals(expectedResult, format.toString());
    }

}
