package com.progressoft.jip9.models;

import java.util.Comparator;

public class TransactionDataModelComparator implements Comparator<TransactionDataModel>{

    @Override
    public int compare(TransactionDataModel firstTransactionDataModel, TransactionDataModel secondTransactionModel) {
        String first = firstTransactionDataModel.getId();
        String second = secondTransactionModel.getId();
        return first.compareTo(second);
    }

}
