package com.progressoft.jip9;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationFilterTest {

    @Test
    public void givenInvalidUsername_whenDoFilter_then() throws IOException, ServletException {
        AuthenticateFilter filter=new AuthenticateFilter();

        HttpServletRequest request= Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response=Mockito.mock(HttpServletResponse.class);
        FilterChain chain=Mockito.mock(FilterChain.class);

        HttpSession session=Mockito.mock(HttpSession.class);
        Mockito.when(request.getContextPath()).thenReturn("");
        Mockito.when(request.getRequestURI()).thenReturn("/sourceUpload");
        Mockito.when(request.getSession()).thenReturn(session);
        filter.doFilter(request,response,chain);

        Mockito.verify(response).sendRedirect("/");
    }
}
